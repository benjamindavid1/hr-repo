package com.infinira.hr.test;
import com.infinira.hr.util.ResourceService;
import com.infinira.hr.util.HrException;
import com.infinira.hr.model.Employee;
import com.infinira.hr.model.Gender;
import com.infinira.hr.model.MaritalStatus;
import com.infinira.hr.model.EmployeeStatus;
import com.infinira.hr.service.HrInterface;
import com.infinira.hr.service.HrService;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

public class HrTest{
    
    private int create(){
        return new HrService().createEmployee(setValues(0,"Selvi","Edwin","E","Jeya","31/12/2000",Gender.m,MaritalStatus.single,"1212","benjamindavid023@gmail.com","12345","b+","31/12/2000","sdds","wewe","trichy","tamil","620010","india","Emp","E:/David Raj.E.pdf","E:/David Raj.E.pdf",EmployeeStatus.active));
    }
    
    private Employee get(int empId) {
        return new HrService().getEmployee(empId);
    }
    
    private List getAll() {
        return new HrService().getAllEmployees();
    }
    
    private void update(int empId){
        new HrService().updateEmployee(setValues(empId,"Kumar","Edwin","E","Jeya","31/12/2000",Gender.m,MaritalStatus.single,"1212","benjamindavid023@gmail.com","12345","b+","31/12/2000","sdds","wewe","trichy","tamil","620010","india","Emp","E:/David Raj.E.pdf","E:/David Raj.E.pdf",EmployeeStatus.active));
    }
    
    private void delete(int empId) {
        new HrService().deleteEmployee(empId);      
    }
    
    public Employee setValues(int empId, String firstName, String middleName, String lastName, String fatherName, String dob, Gender gender, MaritalStatus maritalStatus, String uid, String email, String phone, String bloodGroup, String doj, String addressLine1, String addressLine2, String city, String state, String postalCode, String country, String title, String resume, String photo, EmployeeStatus employeeStatus) {
        Employee employee = new Employee();
        employee.setEmpId(empId);
        employee.setFirstName(firstName);
        employee.setMiddleName(middleName);
        employee.setLastName(lastName);
        employee.setFatherName(fatherName);
        try {
            employee.setDob(new SimpleDateFormat("dd/MM/yyyy").parse(dob));
        } catch (Throwable th){
            throw new HrException("Failed to parse a given DOB", th);
        }
        employee.setGender(gender);
        employee.setMaritalStatus(maritalStatus);
        employee.setUid(uid);
        employee.setEmail(email);
        employee.setPhone(phone);
        employee.setBloodGroup(bloodGroup);
        try {
            employee.setDoj(new SimpleDateFormat("dd/MM/yyyy").parse(doj));
        } catch (Throwable th){
            throw new HrException("Failed to parse a given DOJ", th);
        }
        employee.setAddressLine1(addressLine1);
        employee.setAddressLine2(addressLine2);
        employee.setCity(city);
        employee.setState(state);
        employee.setPostalCode(postalCode);
        employee.setCountry(country);
        employee.setTitle(title);
        employee.setResume(byteArray(new File(resume)));
        employee.setPhoto(byteArray(new File(photo)));
        employee.setEmployeeStatus(employeeStatus);
        return employee;
    }
    private static byte[] byteArray(File file){
        byte[] array = new byte[(int) file.length()];
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            fis.read(array);
            return array;
        } catch (Throwable th){
            throw new HrException("Failed to read a document", th);
        } finally{
            if (fis != null){
                try {
                    fis.close();
                } catch (Throwable th){ }
            }
        }
    }    
    
    public static void main(String[] args) throws Exception {
        HrTest test = new HrTest();        
        int EmployeeID = test.create();
        Employee getEmployee = test.get(146);
        test.update(150);
        test.delete(147);
        List<Employee> ls = test.getAll();
    }
    
}
